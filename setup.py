from distutils.core import setup

with open('README') as f:
    readme = f.read()

setup(name='li6400data',
      version='1.0',
      description='Read data files produced by the LICOR LI-6400.',
      long_description=readme,
      author='Thomas Kluyver',
      author_email='takluyver@kluyver.me.uk',
      url='https://bitbucket.org/takluyver/li-6400-data',
      packages=['li6400data'],
      classifiers = [
        "Intended Audience :: Science/Research",
        "License :: OSI Approved :: BSD License",
        "Programming Language :: Python",
        "Programming Language :: Python :: 2",
        "Programming Language :: Python :: 3",
        "Topic :: Scientific/Engineering",
      ],
     )
