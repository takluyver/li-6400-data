"""The headers of the LICOR file are XML-like, but not valid XML. This module
defines a simple parser for the format.
"""
from pyparsing import *

opentag = Combine(Suppress("<") + Word(alphanums+'[]_') + Suppress(">")).setResultsName('opentag')
closetag = Combine(Suppress("</") + Word(alphanums+'[]_') + Suppress(">")).setResultsName('closetag')

text = Combine(ZeroOrMore(CharsNotIn("<"))).setResultsName('text')
node = Forward()
node << Group(opentag + text \
              + ZeroOrMore(node).setResultsName('children')
              + closetag)

def verify_node(s, l, toks):
    node = toks[0]
    if node.opentag != node.closetag:
        raise ParseFatalException("Tags not matched: %d, %d" % (node.opentag, node.closetag))

node.setParseAction(verify_node)

parse = node.parseString
