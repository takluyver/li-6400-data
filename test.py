from li6400data import LI6400DataSet
import datetime

ld = LI6400DataSet('sample_data')

assert ld.start == datetime.datetime(2012, 9, 7, 16, 32, 53)
assert len(ld.data) == 7
assert len(ld.events) == 11
assert ld.metadata['open']['version'] == '6.2.1'

print("Success!")
